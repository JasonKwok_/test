#!/usr/bin/env escript

-mode(compile).
-define(P(D), io:format(D ++ "\n")).
-define(P(F, D), io:format(F ++ "\n", D)).
-define(ERROR(D), io:format("[error]" ++ D ++ "\n")).
-define(ERROR(F, D), io:format("[error]" ++ F ++ "\n", D)).


main([TabName]) ->
    ModName = io_lib:format("~p.erl", [TabName]),
    FileName = filename:join([OutDir, ModName]),
    FileStr = "this is file named $0 , and here's a testing for using escript to write file",
    ok = file:write_file(FileName, FileStr),
    ok;
main(_) ->
    ?ERROR("用法错误！~s 配置文件 头文件路径 代码路径", [escript:script_name()]).

