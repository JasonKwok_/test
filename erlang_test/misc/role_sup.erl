%%% -------------------------------------------------------------------
%%% name: 角色进程及服务监控树
%%% author: daer
%%% time: 2018-5-22 17:01:01
%%% -------------------------------------------------------------------
-module(role_sup).
-behaviour(supervisor).

-export([start/0]).
-export([start_link/0]).
-export([init/1]).

%%% -------------------------------------------------------------------
%%% api
%%% -------------------------------------------------------------------
%% 启动监控树
start() ->
    {ok, _} = supervisor:start_child(server_sup, {?MODULE, {?MODULE, start_link, []}, transient, infinity, supervisor, [?MODULE]}).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%% -------------------------------------------------------------------
%%% gen_server callbacks
%%% -------------------------------------------------------------------
init([]) ->
    RestartStrategy = one_for_one,
    MaxRestarts = 10,
    MaxSecondsBetweenRestarts = 10,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},


    {ok, {SupFlags, []}}.

%%% -------------------------------------------------------------------
%%% internal functions
%%% -------------------------------------------------------------------
